defmodule Gitlabctl.PushEvent do
  use Ecto.Schema
  import Ecto.Changeset
  alias Gitlabctl.PushEvent


  schema "push_events" do
    field :event, :map

    timestamps()
  end

  @doc false
  def changeset(%PushEvent{} = push_event, attrs) do
    push_event
    |> cast(attrs, [:event])
    |> validate_required([:event])
  end
end
