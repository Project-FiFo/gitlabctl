defmodule Gitlabctl.PipelineEvent do
  use Ecto.Schema
  import Ecto.Changeset
  alias Gitlabctl.PipelineEvent


  schema "pipeline_events" do
    field :event,       :map
    field :pipeline_id, :integer
    field :status,      :string 
    field :ref,         :string 

    timestamps()
  end

  @doc false
  def changeset(%PipelineEvent{} = pipeline_event, attrs) do
    pipeline_event
    |> cast(attrs, [:event, :pipeline_id, :status, :ref])
    |> validate_required([:event, :pipeline_id, :status, :ref])
  end
end

# {
#   "object_kind":"pipeline",
#   "object_attributes":{
#      "status":"pending",
#      "before_sha":"13a77584ecd4b8445c728686f8e47650aa06ceac",
#      "finished_at":null,
#      "created_at":"2017-09-30 23:15:03 UTC",
#      "sha":"13a77584ecd4b8445c728686f8e47650aa06ceac",
#      "tag":false,
#      "duration":null,
#      "stages":[
#         "test"
#      ],
#      "ref":"test",
#      "id":12321988
#   },
#   "project":{
#      "git_ssh_url":"git@gitlab.com:Project-FiFo/FiFo/vmadm.git",
#      "ci_config_path":"",
#      "description":"vmadm for freebsd jails",
#      "avatar_url":null,
#      "default_branch":"dev",
#      "namespace":"FiFo",
#      "git_http_url":"https://gitlab.com/Project-FiFo/FiFo/vmadm.git",
#      "web_url":"https://gitlab.com/Project-FiFo/FiFo/vmadm",
#      "visibility_level":20,
#      "path_with_namespace":"Project-FiFo/FiFo/vmadm",
#      "name":"vmadm"
#   },
#   "user":{
#      "username":"Licenser",
#      "avatar_url":"https://secure.gravatar.com/avatar/d89eaa2301018503e04a7cceea067d50?s=80&d=identicon",
#      "name":"Heinz N. Gies"
#   },
#   "commit":{
#      "url":"https://gitlab.com/Project-FiFo/FiFo/vmadm/commit/13a77584ecd4b8445c728686f8e47650aa06ceac",
#      "timestamp":"2017-09-30T23:15:02+00:00",
#      "message":"Merge branch 'lx-support' into 'test'\n\nfixing not unmounted lx file systems issue https://gitlab.com/Project-FiFo/FiFo/vmadm/issues/48\n\nSee merge request Project-FiFo/FiFo/vmadm!47",
#      "id":"13a77584ecd4b8445c728686f8e47650aa06ceac",
#      "author":{
#         "name":"Heinz N. Gies",
#         "email":"heinz@project-fifo.net"
#      }
#   },
#   "builds":[
#      {
#         "status":"pending",
#         "name":"test",
#         "runner":null,
#         "when":"on_success",
#         "created_at":"2017-09-30 23:15:03 UTC",
#         "manual":false,
#         "user":{
#            "username":"Licenser",
#            "avatar_url":"https://secure.gravatar.com/avatar/d89eaa2301018503e04a7cceea067d50?s=80&d=identicon",
#            "name":"Heinz N. Gies"
#         },
#         "finished_at":null,
#         "artifacts_file":{
#            "filename":null,
#            "size":null
#         },
#         "started_at":null,
#         "id":34621464,
#         "stage":"test"
#      }
#   ]
# }