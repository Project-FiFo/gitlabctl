defmodule Gitlabctl.NoteEvent do
  use Ecto.Schema
  import Ecto.Changeset
  alias Gitlabctl.NoteEvent


  schema "note_events" do
    field :event, :map

    timestamps()
  end

  @doc false
  def changeset(%NoteEvent{} = note_event, attrs) do
    note_event
    |> cast(attrs, [:event])
    |> validate_required([:event])
  end
end
