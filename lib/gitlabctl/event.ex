defmodule Gitlabctl.Event do
  use Ecto.Schema
  import Ecto.Changeset
  alias Gitlabctl.Event


  schema "events" do
    field :event, :map

    timestamps()
  end

  @doc false
  def changeset(%Event{} = event, attrs) do
    event
    |> cast(attrs, [:event])
    |> validate_required([:event])
  end
end
