defmodule Gitlabctl.BuildEvent do
  use Ecto.Schema
  import Ecto.Changeset
  alias Gitlabctl.BuildEvent


  schema "build_events" do
    field :event,    :map
    field :build_id,     :integer
    field :ref,          :string
    field :status,       :string 
    field :project_name, :string 
    field :stage,        :string
    field :name,         :string
    timestamps()
  end

  @doc false
  def changeset(%BuildEvent{} = build_event, attrs) do
    build_event
    |> cast(attrs, [:event, :build_id, :status, :project_name, :stage, :name, :ref])
    |> validate_required([:event, :build_id, :status, :project_name, :stage, :name, :ref])
  end
end

# {
#   "build_id":34621464,
#   "build_status":"created",
#   "project_name":"Project-FiFo / FiFo / vmadm",
#   "before_sha":"13a77584ecd4b8445c728686f8e47650aa06ceac",
#   "object_kind":"build",
#   "repository":{
#      "git_ssh_url":"git@gitlab.com:Project-FiFo/FiFo/vmadm.git",
#      "name":"vmadm",
#      "url":"git@gitlab.com:Project-FiFo/FiFo/vmadm.git",
#      "git_http_url":"https://gitlab.com/Project-FiFo/FiFo/vmadm.git",
#      "visibility_level":20,
#      "homepage":"https://gitlab.com/Project-FiFo/FiFo/vmadm",
#      "description":"vmadm for freebsd jails"
#   },
#   "build_stage":"test",
#   "build_name":"test",
#   "sha":"13a77584ecd4b8445c728686f8e47650aa06ceac",
#   "build_finished_at":null,
#   "tag":false,
#   "user":{
#      "email":"heinz@project-fifo.net",
#      "id":1198364,
#      "name":"Heinz N. Gies"
#   },
#   "build_duration":null,
#   "commit":{
#      "status":"created",
#      "author_email":"heinz@project-fifo.net",
#      "finished_at":null,
#      "author_name":"Heinz N. Gies",
#      "sha":"13a77584ecd4b8445c728686f8e47650aa06ceac",
#      "author_url":"https://gitlab.com/Licenser",
#      "duration":null,
#      "message":"Merge branch 'lx-support' into 'test'\n\nfixing not unmounted lx file systems issue https://gitlab.com/Project-FiFo/FiFo/vmadm/issues/48\n\nSee merge request Project-FiFo/FiFo/vmadm!47",
#      "started_at":null,
#      "id":12321988
#   },
#   "build_started_at":null,
#   "project_id":4216325,
#   "ref":"test",
#   "build_allow_failure":false
# }