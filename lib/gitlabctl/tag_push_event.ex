defmodule Gitlabctl.TagPushEvent do
  use Ecto.Schema
  import Ecto.Changeset
  alias Gitlabctl.TagPushEvent


  schema "tag_push_events" do
    field :event, :map

    timestamps()
  end

  @doc false
  def changeset(%TagPushEvent{} = tag_push_event, attrs) do
    tag_push_event
    |> cast(attrs, [:event])
    |> validate_required([:event])
  end
end
