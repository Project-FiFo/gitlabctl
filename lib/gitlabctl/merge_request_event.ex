defmodule Gitlabctl.MergeRequestEvent do
  use Ecto.Schema
  import Ecto.Changeset
  alias Gitlabctl.MergeRequestEvent


  schema "merge_request_events" do
    field :event, :map

    timestamps()
  end

  @doc false
  def changeset(%MergeRequestEvent{} = merge_request_event, attrs) do
    merge_request_event
    |> cast(attrs, [:event])
    |> validate_required([:event])
  end
end
