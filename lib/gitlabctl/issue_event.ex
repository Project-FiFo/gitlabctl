defmodule Gitlabctl.IssueEvent do
  use Ecto.Schema
  import Ecto.Changeset
  alias Gitlabctl.IssueEvent


  schema "issue_events" do
    field :event, :map

    timestamps()
  end

  @doc false
  def changeset(%IssueEvent{} = issue_event, attrs) do
    issue_event
    |> cast(attrs, [:event])
    |> validate_required([:event])
  end
end
