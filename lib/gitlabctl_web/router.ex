defmodule GitlabctlWeb.Router do
  use GitlabctlWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", GitlabctlWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  scope "/callbacks", GitlabctlWeb do
    pipe_through :api # Use the default browser stack
    post "/webhook.json", PageController, :event
  end

  # Other scopes may use custom stacks.
  # scope "/api", GitlabctlWeb do
  #   pipe_through :api
  # end
end
