defmodule GitlabctlWeb.PageController do
  use GitlabctlWeb, :controller
  alias Gitlabctl.Repo
  alias Gitlabctl.Event
  alias Gitlabctl.PushEvent
  alias Gitlabctl.BuildEvent
  alias Gitlabctl.PipelineEvent


  alias Gitlabctl.MergeRequestEvent
  alias Gitlabctl.NoteEvent
  alias Gitlabctl.IssueEvent
  alias Gitlabctl.TagPushEvent

  def url do
    System.get_env("SLACK_HOOK")
  end

  def index(conn, _params) do
    render conn, "index.html"
  end

  def event(conn, params) do
    expected = System.get_env("GITLAB_TOKEN")
    token = get_req_header(conn, "x-gitlab-token")
    if token == [expected] do
      changeset = event(params)
      Repo.insert!(changeset)
    end
    conn
    |> put_status(:created)
    |> json(%{result: "ok"})
  end

  def event(params = %{"object_kind" => "tag_push"}) do
    TagPushEvent.changeset(%TagPushEvent{}, %{event: params})
  end

  def event(params = %{"object_kind" => "merge_request"}) do
    MergeRequestEvent.changeset(%MergeRequestEvent{}, %{event: params})
  end

  def event(params = %{"object_kind" => "note"}) do
    NoteEvent.changeset(%NoteEvent{}, %{event: params})
  end

  def event(params = %{
  "object_kind" => "issue",
  "project" => %{
    "name" => project_name,
    "web_url" => url,
  },
  "user" => %{
    "name" => user_name,
    "username" => user_login,
    "avatar_url" => avatar_url
  },
  "object_attributes" => %{
    "iid" => issue_id,
    "description" => desc,
    "action" => action,
    "title" => title
  }}) do
    case action do
      "open" ->
        HTTPotion.post(
          url(),
          [body: Poison.encode!(
              %{
                username: "GitLab",
                channel: "#tickets",
                attachments: [
                  %{
                    fallback: "#{project_name}: Issue created",
                    color: "#ff6961",
                    pretext: "#{project_name}: Issue created",
                    author_name: user_name,
                    author_link: "https://gitlab.com/#{user_login}",
                    author_icon: avatar_url,
                    title: "\##{issue_id} #{title}",
                    title_link: "#{url}/issues/#{issue_id}",
                    text: desc,
                    # "fields": [
                    #     {
                    #         "title": "Priority",
                    #         "value": "High",
                    #         "short": false
                    #     }
                    # ],
                    footer: "GitLab bot",
                    footer_icon: "https://s3-us-west-2.amazonaws.com/slack-files2/avatars/2017-10-02/251498382279_2f59fa394b87828fd8f1_48.png"        }]})])
      "close" ->
        HTTPotion.post(
          url(),
          [body: Poison.encode!(
              %{
                username: "GitLab",
                channel: "#tickets",
                attachments: [
                  %{
                    fallback: "#{project_name}: Issue created",
                    color: "#61ff69",
                    pretext: "#{project_name}: Issue closed",
                    author_name: user_name,
                    author_link: "https://gitlab.com/#{user_login}",
                    author_icon: avatar_url,
                    title: "\##{issue_id} #{title}",
                    title_link: "#{url}/issues/#{issue_id}",
                    text: desc,
                    # "fields": [
                    #     {
                    #         "title": "Priority",
                    #         "value": "High",
                    #         "short": false
                    #     }
                    # ],
                    footer: "GitLab bot",
                    footer_icon: "https://s3-us-west-2.amazonaws.com/slack-files2/avatars/2017-10-02/251498382279_2f59fa394b87828fd8f1_48.png"        }]})])
    end
    IssueEvent.changeset(%IssueEvent{}, %{event: params})
  end

  def event(params = %{"object_kind" => "push"}) do
    PushEvent.changeset(%PushEvent{}, %{event: params})
  end

  def event(params = %{
    "object_kind" => "build",
    "build_id" => build_id,
    "build_status" => status,
    "project_name" => project_name,
    "build_stage" => stage,
    "build_name" => name,
    "ref" => ref
    }) do
    BuildEvent.changeset(%BuildEvent{}, %{  
      event: params,
      build_id: build_id,
      status: status,
      project_name: project_name,
      stage: stage,
      name: name,
      ref: ref
      })

  end

  def event(params = %{
  "object_kind" => "pipeline",
  "object_attributes" => %{
    "status" => status,
    "id"     => pipeline_id,
    "ref"    => ref
  },
  "project" => %{
    "name" => project_name,
    "web_url" => url,
  }}) do
    unless status == "pending" do
      emoji = case status do
                "failed" -> ":no_entry:"
                "success" -> ":white_check_mark:"
                "running" -> ":hourglass:"
                "canceled" -> ":heavy_multiplication_x:"
                _ -> "#{pipeline_id}"
              end
      HTTPotion.post(
        url(), [body: Poison.encode!(
                   %{
                     username: "GitLab",
                     channel: "#build",
                     text: "<#{url}/pipelines/#{pipeline_id}|#{emoji}> *#{project_name}* #{ref} *#{status}*"
                   })])
    end
    PipelineEvent.changeset(%PipelineEvent{}, %{
          event: params,
        pipeline_id: pipeline_id,
        status: status,
        ref: ref
        })
  end

  def event(params) do
    Event.changeset(%Event{}, %{event: params})
  end

end
