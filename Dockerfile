FROM elixir:latest

RUN curl -sL https://deb.nodesource.com/setup_7.x | bash - \
    && apt-get install -y nodejs \
    && apt-get clean
COPY assets /app/assets
COPY config /app/config
COPY priv /app/priv
COPY lib /app/lib
COPY mix.exs /app/mix.exs
COPY mix.lock /app/mex.lock

COPY docker/prod.secret.exs /app/config/prod.secret.exs

ENV MIX_ENV=prod
RUN cd /app \
    && mix local.hex --force \
    && mix local.rebar --force \
    && mix deps.get \
    && env MIX_ENV=prod mix compile \
    && cd /app/assets \
    && npm install \
    && ./node_modules/.bin/brunch build \
    && rm -r node_modules \
    && cd /app \
    && env MIX_ENV=prod mix phx.digest \
    && rm -rf root/.cache \
    && rm -rf root/.npm

COPY docker/docker-entrypoint.sh /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]
