defmodule Gitlabctl.Repo.Migrations.AddPipelineEventFields do
  use Ecto.Migration

  def change do
    alter table("pipeline_events") do
      add :pipeline_id, :integer
      add :status,      :text 
      add :ref,         :text 
    end  
  end
end
