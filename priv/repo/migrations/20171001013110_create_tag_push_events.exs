defmodule Gitlabctl.Repo.Migrations.CreateTagPushEvents do
  use Ecto.Migration

  def change do
    create table(:tag_push_events) do
      add :event, :map

      timestamps()
    end

  end
end
