defmodule Gitlabctl.Repo.Migrations.RenameBuildIdFields do
  use Ecto.Migration

  def change do
      rename table("build_events"), :build_status,  to: :status
      rename table("build_events"), :build_stage,   to: :stage
      rename table("build_events"),:build_name,    to: :name
  end
end
