defmodule Gitlabctl.Repo.Migrations.CreateEvents do
  use Ecto.Migration

  def change do
    create table(:events) do
      add :event, :map

      timestamps()
    end

  end
end
