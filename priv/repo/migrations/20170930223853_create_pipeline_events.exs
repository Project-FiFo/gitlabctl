defmodule Gitlabctl.Repo.Migrations.CreatePipelineEvents do
  use Ecto.Migration

  def change do
    create table(:pipeline_events) do
      add :event, :map

      timestamps()
    end

  end
end
