defmodule Gitlabctl.Repo.Migrations.AddRefToBuild do
  use Ecto.Migration

  def change do
    alter table("build_events") do
      add :ref,  :text 
    end
  end
end
