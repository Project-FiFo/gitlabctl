defmodule Gitlabctl.Repo.Migrations.CreateBuildEvents do
  use Ecto.Migration

  def change do
    create table(:build_events) do
      add :event, :map

      timestamps()
    end

  end
end
