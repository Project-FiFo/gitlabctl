defmodule Gitlabctl.Repo.Migrations.AddBuildEventFields do
  use Ecto.Migration

  def change do
    alter table("build_events") do
      add :build_id,      :integer
      add :build_status,  :text 
      add :project_name,  :text 
      add :build_stage,   :text
      add :build_name,    :text
    end
  end
end