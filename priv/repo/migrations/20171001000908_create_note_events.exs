defmodule Gitlabctl.Repo.Migrations.CreateNoteEvents do
  use Ecto.Migration

  def change do
    create table(:note_events) do
      add :event, :map

      timestamps()
    end

  end
end
