defmodule Gitlabctl.Repo.Migrations.CreateMergeRequestEvents do
  use Ecto.Migration

  def change do
    create table(:merge_request_events) do
      add :event, :map

      timestamps()
    end

  end
end
