defmodule Gitlabctl.Repo.Migrations.CreatePushEvents do
  use Ecto.Migration

  def change do
    create table(:push_events) do
      add :event, :map

      timestamps()
    end

  end
end
