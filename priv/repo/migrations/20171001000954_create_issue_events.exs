defmodule Gitlabctl.Repo.Migrations.CreateIssueEvents do
  use Ecto.Migration

  def change do
    create table(:issue_events) do
      add :event, :map

      timestamps()
    end

  end
end
