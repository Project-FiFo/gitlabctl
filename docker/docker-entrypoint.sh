#!/bin/bash
set -e
set -x

export MIX_ENV=prod

## This is ugly!
sleep ${TIMEOUT}

cd /app
## We run create migrate and seed
## if the dagtabase exists, create will fail and no seeding is done
MIX_ENV=prod mix ecto.create
MIX_ENV=prod MIX_ENV=prod mix ecto.migrate

## Now start the server
MIX_ENV=prod elixir --name gitlabctl@127.0.0.1 --cookie gitlabctl -S mix phx.server
